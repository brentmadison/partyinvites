using System;
using System.ComponentModel.DataAnnotations;

namespace PartyInvites1.Models
{
    public class GuestResponse
    {

        public GuestResponse()
        {

        }
        public GuestResponse(string name, string email, string phone, bool? willAttend)
        {
            Name = name;
            Email = email;
            Phone = phone;
            WillAttend = willAttend;
        }

        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your phone")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter if you'll attend")]
        public bool? WillAttend { get; set; }
    }
}