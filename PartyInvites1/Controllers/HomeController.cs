﻿using Microsoft.AspNetCore.Mvc;
using PartyInvites1.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PartyInvites1.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            return View("MainView");
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }

        public ViewResult RsvpForm(GuestResponse response)
        {
            Boolean finalWillAttend = response.WillAttend.HasValue ? response.WillAttend.Value : false;
            var random = new Random();
            List<string> acceptedResponses = new List<string> { "Right on, ", "It's going to be a blast, ", "Thanks for coming, " };
            List<string> declinedResponses = new List<string> { "Sorry to hear that, ", "We'll see you next time, ", "We'll miss you, " };
            string finalResponse = finalWillAttend ? acceptedResponses[random.Next(acceptedResponses.Count)] : declinedResponses[random.Next(acceptedResponses.Count)];
            //Randomize accepted/decline responses
            if (ModelState.IsValid)
            {
                ViewBag.finalResponse = finalResponse;
                ViewBag.nameColor = finalWillAttend ? "lime" : "red";
                Repository.AddResponse(response);
                return View("Thanks", response);
            } else
            {
                return View();
            }
        }

        public ViewResult ListResponses()
        {
            return View(Repository.Responses.Where(r => r.WillAttend == true));
        }


        [HttpGet]
        public ViewResult TestingThanksRender()
        {
            GuestResponse testResponse = new GuestResponse("testName", "testEmail", "9153126114", true);
            ViewBag.finalResponse = "Just testing, ";
            ViewBag.nameColor = "lime";

            return View("Thanks", testResponse);
        }

        [HttpGet]
        public ViewResult TestingResponseRender()
        {
            GuestResponse testResponse = new GuestResponse("testName", "testEmail", "9153126114", true);
            GuestResponse testResponse1 = new GuestResponse("testName", "testEmail", "9153126114", true);

            List<GuestResponse> list = new List<GuestResponse>();
            list.Add(testResponse);
            list.Add(testResponse1);
            return View("ListResponses", list);
        }
    }
}
